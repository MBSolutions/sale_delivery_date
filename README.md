Master

[![Pipeline status](https://gitlab.com/m9s/sale_delivery_date/badges/master/pipeline.svg)](https://gitlab.com/m9s/sale_delivery_date/commits/master)

Develop

[![Pipeline status](https://gitlab.com/m9s/sale_delivery_date/badges/develop/pipeline.svg)](https://gitlab.com/m9s/sale_delivery_date/commits/develop)

[![Coverage report](https://gitlab.com/m9s/sale_delivery_date/badges/develop/coverage.svg)](http://m9s.gitlab.io/sale_delivery_date)



This module runs with the Tryton application platform.

Installing
----------

See INSTALL

Note
----

This module is developed and tested over a patched Tryton server and
core modules. Maybe some of these patches are required for the module to work.

Support
-------

For more information or if you encounter any problems with this module,
please contact the programmers at

#### MBSolutions

   * Issues:   https://gitlab.com/m9s/sale_delivery_date/issues
   * Website:  http://www.m9s.biz/
   * Email:    info@m9s.biz

If you encounter any problems with Tryton, please don't hesitate to ask
questions on the Tryton bug tracker, forum or IRC channel:

   * http://bugs.tryton.org/
   * http://www.tryton.org/forum
   * irc://irc.freenode.net/tryton

License
-------

See LICENSE

Copyright
---------

See COPYRIGHT

